import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostEstimatingComponent } from './cost-estimating.component';

describe('CostEstimatingComponent', () => {
  let component: CostEstimatingComponent;
  let fixture: ComponentFixture<CostEstimatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostEstimatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostEstimatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
