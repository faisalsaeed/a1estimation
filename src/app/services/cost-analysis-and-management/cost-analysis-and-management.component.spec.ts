import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostAnalysisAndManagementComponent } from './cost-analysis-and-management.component';

describe('CostAnalysisAndManagementComponent', () => {
  let component: CostAnalysisAndManagementComponent;
  let fixture: ComponentFixture<CostAnalysisAndManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostAnalysisAndManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostAnalysisAndManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
