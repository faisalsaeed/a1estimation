import { Component } from '@angular/core';
import { OnInit  } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'a1est-angular';

  onActivate(event) {
    window.scrollTo(0,  0);
  }
  

}
